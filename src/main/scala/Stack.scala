sealed trait Stack[+T]{
  override def toString = s"Stack[${toStringHelper(this)}]"

  def toStringHelper[E>:T](stack: Stack[E]): String = stack match {
    case Nil => ""
    case Node(head, Nil) => head.toString
    case Node(head, tail) => head.toString + "," + toStringHelper(tail)
  }
}
case object Nil extends Stack[Nothing]
case class Node[+T](head: T, tail: Stack[T]) extends Stack[T]

object Stack extends {

  def empty: Nil.type = Nil

  def isEmpty[E](stack: Stack[E]): Boolean = stack match {
    case Nil => true
    case Node(_, _) => false
  }

  def cons[E](elem: E, stack: Stack[E]): Stack[E] = Node(elem, stack)

  def head[E](stack: Stack[E]): E = stack match {
    case Nil => throw new RuntimeException("Head of empty collection")
    case Node(head, _) => head
  }

  def tail[E](stack: Stack[E]) : Stack[E] = stack match {
    case Nil => throw new RuntimeException("Tail of empty collection")
    case Node(_, tail) => tail
  }

  def concat[E](left: Stack[E], right: Stack[E]) : Stack[E] = left match {
    case Nil => right
    case Node(head, tail) => cons(head, concat(tail, right))
  }

  def insert[E](stack: Stack[E], index: Int, value: E): Stack[E] = index match {
    case 0 => cons(value, stack)
    case _ => stack match {
      case Nil => throw new RuntimeException("Index out of bounds")
      case Node(head, tail) => cons(head, insert(tail, index - 1, value))
    }
  }

  def update[E](stack: Stack[E], index: Int, value: E): Stack[E] = index match {
    case 0 => stack match {
      case Nil => throw new RuntimeException("Stack is empty")
      case Node(_, tail) => cons(value, tail)
    }
    case _ => stack match {
      case Nil => throw new RuntimeException("Index out of bounds")
      case Node(head, tail) => cons(head, update(tail, index - 1, value))
    }
  }

  def length[E](stack: Stack[E]): Int = {
    stack match {
      case Nil => 0
      case Node(_, tail) => 1 + length(tail)
    }
  }

  def suffixes[E](stack: Stack[E]): Stack[Stack[E]] = {
    stack match {
      case Nil => Nil
      case s@Node(_, tail) => concat(cons(s, Nil), suffixes(tail))
    }
  }
}


object Main{
  import Stack._
  def main(args: Array[String]): Unit = {
    val stack = Node(1, Node(2, Nil))
    val stack2 = Node(3, Node(4, Nil))
    val bigStack = concat(stack, stack2)
    print(suffixes(bigStack))
  }
}

