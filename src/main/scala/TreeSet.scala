sealed trait TreeSet[+E] {
  def empty: Leaf.type = Leaf

  def isEmpty: Boolean

  def insert[T >: E](value: T)(implicit el: Ordering[T]): TreeSet[T]

  def contains[T >: E](i: T)(implicit el: Ordering[T]): Boolean

  def containsFast[T >: E](i: T)(implicit el: Ordering[T]): Boolean

  def containsWithSentinel[T >: E](i: T, sentinel: Option[T] = None)(implicit ordArg: Ordering[T]): Boolean

}

case object Leaf extends TreeSet[Nothing] {
  def contains[T](i: T)(implicit el: Ordering[T]) = false

  override def isEmpty: Boolean = true

  override def insert[T](value: T)(implicit el: Ordering[T]): TreeSet[T] = Tree(Leaf, value, Leaf)

  def containsFast[T](i: T)(implicit el: Ordering[T]): Boolean = false

  def containsWithSentinel[T](i: T, sentinel: Option[T] = None)(implicit ordArg: Ordering[T]): Boolean = sentinel.contains(i)
}

case class Tree[+E](left: TreeSet[E], elem: E, right: TreeSet[E])(implicit ord: Ordering[E]) extends TreeSet[E] {
  override def isEmpty: Boolean = false

  def insert[T >: E](value: T)(implicit ordArg: Ordering[T]): TreeSet[T] = if (ordArg.lt(value, elem)) {
    Tree(left.insert(value), elem, right)
  } else if (ordArg.gt(value, elem)) {
    Tree(left, elem, right.insert(value))
  } else {
    this
  }

  override def contains[T >: E](i: T)(implicit ordArg: Ordering[T]): Boolean = if(ordArg.lt(i, elem)){
    left.contains(i)
  } else if(ordArg.gt(i, elem)) {
    right.contains(i)
  } else {
    true
  }

  def containsFast[T >: E](i: T)(implicit el: Ordering[T]): Boolean = containsWithSentinel(i)

  def containsWithSentinel[T >: E](i: T, sentinel: Option[T] = None)(implicit ordArg: Ordering[T]): Boolean = if(ordArg.lt(i, elem)){
    left.containsWithSentinel(i)
  } else {
    right.containsWithSentinel(i, Some(elem))
  }
}


object TestMain {
  def main(args: Array[String]): Unit = {
    val tree = Leaf.insert(5).insert(4).insert(6)
    print(tree)
  }
}