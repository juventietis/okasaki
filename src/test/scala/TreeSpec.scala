import org.scalatest.{FlatSpec, Matchers}


class TreeSpec extends FlatSpec with Matchers {
  "insert" should "insert an element into an empty tree" in {
    Leaf.insert(1) should be(Tree(Leaf, 1, Leaf))
  }

  it should "insert an element into an existing tree respecting ordering" in {
    Tree(Leaf, 5, Leaf).insert(4) should be(Tree(Tree(Leaf, 4, Leaf), 5, Leaf))

    Tree(Leaf, 5, Leaf).insert(6) should be(Tree(Leaf, 5, Tree(Leaf, 6, Leaf)))

    Tree(Leaf, 5, Leaf).insert(6).insert(4) should be(Tree(Tree(Leaf, 4, Leaf), 5, Tree(Leaf, 6, Leaf)))

    Tree(Leaf, 5, Leaf).insert(7).insert(6) should be(Tree(Leaf, 5, Tree(Tree(Leaf, 6, Leaf), 7, Leaf)))
  }

  it should "should return the tree if element already exist" in {
    Tree(Leaf, 5, Leaf).insert(5) should be(Tree(Leaf, 5, Leaf))
  }

  "contains" should "return false if tree is empty" in {
    Leaf.contains(5) should be (false)
  }

  it should "return false if tree has one element which is different" in {
    Tree(Leaf, 4,Leaf).contains(5) should be (false)
  }

  it should "return true if tree has one element which matches" in {
    Tree(Leaf, 5,Leaf).contains(5) should be (true)
  }

  it should "return true if tree is complicated an contains the element" in {
    Tree(Tree(Leaf, 4, Leaf), 5, Tree(Leaf, 6, Leaf)).contains(4) should be (true)

    Tree(Tree(Leaf, 4, Leaf), 5, Tree(Leaf, 6, Leaf)).contains(6) should be (true)
  }

  it should "return false if tree is complicated an does not contain the element" in {
    Tree(Tree(Leaf, 4, Leaf), 5, Tree(Leaf, 6, Leaf)).contains(3) should be (false)

    Tree(Tree(Leaf, 4, Leaf), 5, Tree(Leaf, 6, Leaf)).contains(7) should be (false)
  }

  "containsFast" should "return true if tree has an element which matches" in {
    Tree(Leaf, 5,Leaf).containsFast(5) should be (true)

    Tree(Tree(Leaf, 4, Leaf), 5, Leaf).containsFast(5) should be (true)

    Tree(Tree(Leaf, 4, Leaf), 5, Tree(Tree(Leaf, 6, Leaf), 8, Leaf)).containsFast(6) should be (true)
  }

  it should "return true if tree does not have an element which matches" in {
    Leaf.containsFast(5) should be (false)

    Tree(Tree(Leaf, 4, Leaf), 5, Leaf).containsFast(3) should be (false)

    Tree(Tree(Leaf, 3, Leaf), 5, Leaf).containsFast(2) should be (false)

    Tree(Tree(Leaf, 3, Leaf), 5, Leaf).containsFast(4) should be (false)

    Tree(Tree(Leaf, 4, Leaf), 5, Tree(Tree(Leaf, 6, Leaf), 8, Leaf)).containsFast(7) should be (false)
  }

}
