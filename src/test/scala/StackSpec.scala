import org.scalatest._
import Stack._
class StackSpec extends  FlatSpec with Matchers{
  "isEmpty" should "return true when stack is empty" in {
    isEmpty(Stack.empty) should be (true)
  }

  "isEmpty" should "return false when stack is not empty" in {
    isEmpty(Node(5, Nil)) should be (false)
  }

  "cons" should "return a stack with the new element added" in {
    val stack = Node(5, Nil)
    cons(4, stack) should be (Node(4, Node(5, Nil)))
  }

  "insert" should "insert the element at the right place in the stack correctly" in {
    val stack = Node(5, Node(4, Node(3, Nil)))
    insert(stack, 0, 1) should be (Node(1, Node(5, Node(4, Node(3, Nil)))))
    insert(stack, 1, 1) should be (Node(5, Node(1, Node(4, Node(3, Nil)))))
    insert(stack, 3, 1) should be (Node(5, Node(4, Node(3, Node(1, Nil)))))
    a [RuntimeException] should be thrownBy {
      insert(stack, 4, 1)
    }
  }

  "update" should "update the stack correctly" in {
    val stack = Node(5, Node(4, Node(3, Nil)))
    update(stack, 0, 1) should be (Node(1, Node(4, Node(3, Nil))))
    update(stack, 1, 1) should be (Node(5, Node(1, Node(3, Nil))))
    update(stack, 2, 1) should be (Node(5, Node(4, Node(1, Nil))))
    a [RuntimeException] should be thrownBy {
      update(stack, 3, 1)
    }
  }

  "length" should "return the correct length" in {
    Stack.length(Nil) should be (0)
    Stack.length(Node(5, Nil)) should be (1)
    Stack.length(Node(5, Node(4, Nil))) should be (2)

  }
}
